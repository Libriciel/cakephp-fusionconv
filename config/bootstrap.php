<?php
declare(strict_types=1);

use Cake\Core\Configure;

/**
 * File bootstrap
 *
 * phpgedooo_client : Client php pour l'utilisation du serveur gedooo
 * Copyright (c) Libriciel SCOP <http://www.libriciel.fr>
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP <http://www.libriciel.fr>
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 * @version     2.0.0
 */
if (!defined('GEDOOO_REST') && Configure::check('FusionConv.Fusion.Type') && Configure::read('FusionConv.Fusion.Type') === 'Gedooo') {
    define('GEDOOO_REST', 'http://' . Configure::read('FusionConv.Fusion.host') . ':' . Configure::read('FusionConv.Fusion.port'). '/ODFgedooo');
}
