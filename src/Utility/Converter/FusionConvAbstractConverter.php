<?php
declare(strict_types=1);

namespace FusionConv\Utility\Converter;

use Cake\Core\Configure;
use Cake\Utility\Hash;

/**
 * FusionConvAbstractConverter class
 *
 * PHP 7.3
 *
 * @package FusionConv
 * @subpackage src.Utility.Converter
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
abstract class FusionConvAbstractConverter implements FusionConvConverterInterface
{
    /**
     * Returns the value read in the path of self::$configure under the key
     * $configureKey or the default value, as long as the value is null
     *
     * @param array $configured Foo
     * @param string $configureKey Foo
     * @param mixed $currentValue Foo
     * @return mixed
     * @todo Using the plugin Configured, which remains to write
     */
    protected static function _configured(array $configured, string $configureKey, $currentValue = null)
    {
        if (is_null($currentValue)) {
            $configuredPath = Hash::get($configured, "{$configureKey}.path");
            $currentValue = Configure::read($configuredPath);

            if (is_null($currentValue)) {
                $currentValue = Hash::get($configured, "{$configureKey}.default");
            }
        }

        return $currentValue;
    }
}
