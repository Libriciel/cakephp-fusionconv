<?php
declare(strict_types=1);

namespace FusionConv\Utility\Converter;

use Cake\Log\Log;
use PhpXmlRpc\Client;
use PhpXmlRpc\Request;
use PhpXmlRpc\Value;

/**
 * FusionConvConverterCloudooo class is able to convert a file from a format to another
 * (by default, odt to pdf) using a cloudooo server
 *
 * @package FusionConv
 * @subpackage Utility.Converter
 */
abstract class FusionConvConverterCloudooo extends FusionConvAbstractConverter
{
    /**
     * Cloudooo server address.
     *
     * @var string|null
     */
    protected static ?string $_server = null;

    /**
     * Cloudooo server port.
     *
     * @var int|null
     */
    protected static ?int $_port = null;

    /**
     * Cloudooo server proxy host.
     *
     * @var string|null
     */
    protected static ?string $_proxyHost = null;

    /**
     * Cloudooo server proxy port.
     *
     * @var string|null
     */
    protected static ?string $_proxyPort = null;

    /**
     * Cloudooo server proxy username.
     *
     * @var string|null
     */
    protected static ?string $_proxyUsername = null;

    /**
     * Cloudooo server proxy password.
     *
     * @var string|null
     */
    protected static ?string $_proxyPassword = null;

    /**
     * Cloudooo server use curl.
     *
     * @var int|null
     */
    protected static ?int $_useCurl = 2;

    /**
     * Paths to variables in CakePHP config :
     *  - FusionConv.FusionConvConverterCloudooo.xml_rpc_class (integer, default 1, value 1 or 2)
     *  - FusionConv.FusionConvConverterCloudooo.server (string, default 127.0.01)
     *  - FusionConv.FusionConvConverterCloudooo.port (integer, default 8011)
     *
     * @todo defaultInputFormat, defaultOutputFormat ?
     * @var array
     */
    public static array $configured = [
        'server' => [
            'path' => 'FusionConv.Conversion.host',
            'type' => 'string',
            'default' => '127.0.0.1',
        ],
        'port' => [
            'path' => 'FusionConv.Conversion.port',
            'type' => 'string',
            'default' => '8011',
        ],
        'proxyHost' => [
            'path' => 'FusionConv.Conversion.proxyHost',
            'type' => 'string',
            'default' => null,
        ],
        'proxyPort' => [
            'path' => 'FusionConv.Conversion.proxyPort',
            'type' => 'string',
            'default' => null,
        ],
        'proxyUsername' => [
            'path' => 'FusionConv.Conversion.proxyUsername',
            'type' => 'string',
            'default' => null,
        ],
        'proxyPassword' => [
            'path' => 'FusionConv.Conversion.proxyPassword',
            'type' => 'string',
            'default' => null,
        ],
        'useCurl' => [
            'path' => 'FusionConv.Conversion.useCurl',
            'type' => 'int',
            'default' => 2,
        ],
    ];

    /**
     * Initialization: if server or port aren't specified, we
     * try to read they're values in config or we take
     * default values
     *
     * @see self::$configure
     */
    protected static function _init()
    {
        self::$_server = self::_configured(self::$configured, 'server', self::$_server);
        self::$_port = (int)self::_configured(self::$configured, 'port', self::$_port);
        self::$_proxyHost = self::_configured(self::$configured, 'proxyHost', self::$_proxyHost);
        self::$_proxyPort = self::_configured(self::$configured, 'proxyPort', self::$_proxyPort);
        self::$_proxyUsername = self::_configured(self::$configured, 'proxyUsername', self::$_proxyUsername);
        self::$_proxyPassword = self::_configured(self::$configured, 'proxyPassword', self::$_proxyPassword);
        self::$_useCurl = (int)self::_configured(self::$configured, 'useCurl', self::$_useCurl);
    }

    /**
     * Prepares conversion by sending message to cloudooo server
     *
     * @param string $content File content to convert
     * @param string $inputFormat Input file format
     * @param string $outputFormat Output file format
     * @return string
     */
    protected static function _xmlRpcConvert(string $content, string $inputFormat, string $outputFormat)
    {
        $params = new Request('convertFile', [
            new Value(base64_encode($content), 'string'),
            new Value($inputFormat, 'string'),
            new Value($outputFormat, 'string'),
            new Value(false, 'boolean'),
            new Value(true, 'boolean')
        ]);

        $url = 'http://' . self::$_server .':'. self::$_port;
        $client = new Client($url);
        $client->setUseCurl(self::$_useCurl);
        if(!empty(self::$_proxyHost)) {
            $client->setProxy(self::$_proxyHost, self::$_proxyPort, self::$_proxyUsername, self::$_proxyPassword);
        }
        $response = $client->send($params);
        $msgid = 'Erreur du serveur Cloudooo "%s:%d": %s';
        if (empty($response)) {
            Log::error(sprintf($msgid, self::$_server, self::$_port, $client->setDebug(3)), LOG_ERR);

            return false;
        }

        if (empty($response->value())) {
            Log::error(sprintf($msgid, self::$_server, self::$_port, $response->faultString()), LOG_ERR);

            return false;
        }

        return base64_decode($response->value()->scalarval());
    }

    /**
     * File content initialization and converting from odt to pdf
     *
     * @param string $content File content to convert
     * @param string $inputFormat Input file format
     * @param string $outputFormat Output file format
     * @return string
     */
    public static function convert(string $content, $inputFormat = 'odt', $outputFormat = 'pdf'): string
    {
        self::_init();

        return self::_xmlRpcConvert($content, $inputFormat, $outputFormat);
    }
}
