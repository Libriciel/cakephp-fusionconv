<?php
declare(strict_types=1);

namespace FusionConv\Utility\Converter;

/**
 * Interface for FusionConvAbstractConverter downward classes.
 *
 * @package FusionConv
 * @subpackage src.Utility.Converter
 */
interface FusionConvConverterInterface
{
    /**
     * Initialization et converting file content from a format to another
     *
     * @param string $content file content to convert
     * @param string $inputFormat File input format
     * @param string $outputFormat File output format
     * @return string
     */
    public static function convert(string $content, $inputFormat = 'odt', $outputFormat = 'pdf'): string;
}
