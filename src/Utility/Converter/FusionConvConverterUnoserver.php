<?php
declare(strict_types=1);

namespace FusionConv\Utility\Converter;

use CURLFile;
use CURLStringFile;

/**
 * FusionConvConverterUnoserver class is able to convert a file from a format to another
 * (by default, odt to pdf) using a unoserver server.
 *
 * @package FusionConv
 * @subpackage Utility.Converter
 */
abstract class FusionConvConverterUnoserver extends FusionConvAbstractConverter
{
    /**
     * Unoserver address.
     *
     * @var string|null
     */
    protected static ?string $_server = null;

    /**
     * Unoserver port.
     *
     * @var int|null
     */
    protected static ?int $_port = null;

    /**
     * Unoserver proxy host.
     *
     * @var string|null
     */
    protected static ?string $_proxyHost = null;

    /**
     * Unoserver proxy port.
     *
     * @var string|null
     */
    protected static ?string $_proxyPort = null;

    /**
     * Unoserver proxy username.
     *
     * @var string|null
     */
    protected static ?string $_proxyUsername = null;

    /**
     * Unoserver proxy password.
     *
     * @var string|null
     */
    protected static ?string $_proxyPassword = null;

    /**
     * Unoserver use curl.
     *
     * @var int|null
     */
    protected static ?int $_useCurl = 2;

    /**
     * Unoserver use curl.
     *
     * @var bool|null
     */
    protected static ?bool $_useStream = true;

    /**
     * Paths to variables in CakePHP config :
     *  - FusionConv.Unoserver.host (string, default 127.0.0.1)
     *  - FusionConv.Unoserver.port (integer, default 2004)
     *  - FusionConv.Unoserver.useStream (boolean, default true)
     *
     * @var array
     */
    public static array $configured = [
        'server' => [
            'path' => 'FusionConv.Unoserver.host',
            'type' => 'string',
            'default' => '127.0.0.1',
        ],
        'port' => [
            'path' => 'FusionConv.Unoserver.port',
            'type' => 'string',
            'default' => '2004',
        ],
        'proxyHost' => [
            'path' => 'FusionConv.Unoserver.proxyHost',
            'type' => 'string',
            'default' => null,
        ],
        'proxyPort' => [
            'path' => 'FusionConv.Unoserver.proxyPort',
            'type' => 'string',
            'default' => null,
        ],
        'proxyUsername' => [
            'path' => 'FusionConv.Unoserver.proxyUsername',
            'type' => 'string',
            'default' => null,
        ],
        'proxyPassword' => [
            'path' => 'FusionConv.Unoserver.proxyPassword',
            'type' => 'string',
            'default' => null,
        ],
        'useCurl' => [
            'path' => 'FusionConv.Unoserver.useCurl',
            'type' => 'int',
            'default' => 2,
        ],
        'useStream' => [
            'path' => 'FusionConv.Unoserver.useStream',
            'type' => 'bool',
            'default' => true,
        ],
    ];

    /**
     * Initialization: if server or port aren't specified, we
     * try to read they're values in config or we take
     * default values
     *
     * @see self::$configure
     */
    protected static function _init()
    {
        self::$_server = self::_configured(self::$configured, 'server', self::$_server);
        self::$_port = (int)self::_configured(self::$configured, 'port', self::$_port);
        self::$_proxyHost = self::_configured(self::$configured, 'proxyHost', self::$_proxyHost);
        self::$_proxyPort = self::_configured(self::$configured, 'proxyPort', self::$_proxyPort);
        self::$_proxyUsername = self::_configured(self::$configured, 'proxyUsername', self::$_proxyUsername);
        self::$_proxyPassword = self::_configured(self::$configured, 'proxyPassword', self::$_proxyPassword);
        self::$_useCurl = (int)self::_configured(self::$configured, 'useCurl', self::$_useCurl);
        self::$_useStream = (bool)self::_configured(self::$configured, 'useStream', self::$_useStream);
    }

    /**
     * File content initialization and converting from odt to pdf
     *
     * @param string $content File content to convert
     * @param string $inputFormat Input file format
     * @param string $outputFormat Output file format
     * @return string
     * @throws \Throwable
     */
    public static function convert(string $content, $inputFormat = 'odt', $outputFormat = 'pdf'): string
    {
        self::_init();

        $tempnam = null;
        $basename = 'document.odt';
        $mimetype = 'application/vnd.oasis.opendocument.text';

        try {
            if (static::$_useStream) {
                // @see: https://php.watch/versions/8.1/CURLStringFile
                $file = new CURLStringFile($content, $mimetype, $basename);
            } else {
                $tempnam = tempnam(sys_get_temp_dir(), 'FusionConvConverterUnoserver_');
                file_put_contents($tempnam, $content);
                $file = new CURLFile($tempnam, $mimetype, $basename);
            }

            $ch = curl_init();
            $options = [
                CURLOPT_URL => sprintf('http://%s:%s/request', self::$_server, self::$_port),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => 1,
                CURLOPT_VERBOSE => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_HTTPHEADER => [
                    'Content-Type:multipart/form-data'
                ],
                CURLOPT_POSTFIELDS => [
                    'convert-to' => 'pdf',
                    'file' => $file,
                ],
            ];

            if(!empty(self::$_proxyHost)) {
                curl_setopt($ch, CURLOPT_PROXY, static::$_proxyHost . ':' . static::$_proxyPort);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, static::$_proxyUsername . ':' . static::$_proxyPassword);
            }

            curl_setopt_array($ch, $options);
            $result = curl_exec($ch);
            curl_close($ch);
        } catch (\Throwable $exc) {
            throw $exc;
        } finally {
            if ($tempnam !== null) {
                unlink($tempnam);
            }
        }

        return $result;
    }
}
