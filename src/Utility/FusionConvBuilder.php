<?php
declare(strict_types=1);

namespace FusionConv\Utility;

use Cake\Utility\Hash;
use phpgedooo_client\GDO_ContentType;
use phpgedooo_client\GDO_FieldType;
use phpgedooo_client\GDO_IterationType;
use phpgedooo_client\GDO_PartType;

/**
 * FusionConvBuilder class
 *
 * @package FusionConv
 * @subpackage src.Utility
 */
abstract class FusionConvBuilder
{
    /**
     * Builds pdf file by analyzing if values are files or texts,
     * in a GDO_PartType
     *
     * @param GDO_PartType $GDOPartType Foo
     * @param array $data Foo
     * @param array $types Foo
     * @param array $correspondances Foo
     * @return \GDO_PartType
     */
    public static function main(GDO_PartType $GDOPartType, array $data, array $types, array $correspondances)
    {
        foreach ($correspondances as $newKey => $alias) {
            $aliasSplit = explode('.', $newKey);
            $count = count($aliasSplit);

            $keyExists = (
                    ( $count == 1 && array_key_exists($alias, $data) ) || ( $count == 2 && array_key_exists($aliasSplit[0], $data) && array_key_exists($aliasSplit[1], $data[$aliasSplit[0]]) )
                    // INFO: Hash::check doesn't work very well if value is null
                    || Hash::check($data, $alias)
                    );

            if ($keyExists) {
                $value = Hash::get($data, $alias);
                $type = isset($types[$newKey]) ? $types[$newKey] : 'text';

                if ($type == 'file') {
                    $GDOPartType->addElement(new GDO_ContentType($alias, $alias . '.odt', 'application/vnd.oasis.opendocument.text', 'binary', $value));
                } else {
                    $value = mb_convert_encoding($value, 'UTF-8', mb_detect_encoding($value));
                    $GDOPartType->addElement(new GDO_FieldType($alias, $value, $type));
                }
            }
        }

        return $GDOPartType;
    }

    /**
     * Adds new content to the $GDOPartType already built above.
     *
     * @param GDO_PartType $MainPart Foo
     * @param string $iterationName Foo
     * @param array $datas Foo
     * @param array $types Foo
     * @param array $correspondances Foo
     * @return \GDO_PartType
     */
    public static function iteration(GDO_PartType $MainPart, $iterationName, array $datas, array $types, array $correspondances)
    {
        // $iterationName = 'name';
        $Iteration = new GDO_IterationType($iterationName);
        foreach ($datas[$iterationName] as $data) {
            $InnerPart = new GDO_PartType();
            $InnerPart = self::main($InnerPart, $data, $types, $correspondances);
            foreach ($data as $key_iteration => $value) {
                //$InnerPart = new GDO_PartType();
                if (is_array($value)) {
                    self::iteration($InnerPart, $key_iteration, array($key_iteration => $value), $types, $correspondances);
                }
            }
            $Iteration->addPart($InnerPart);
        }

        $MainPart->addElement($Iteration);

        return $MainPart;
    }
}
