<?php


use Cake\Log\Log;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;


if( !defined( 'GEDOOO_PLUGIN_DIR' ) ) {
    define( 'GEDOOO_PLUGIN_DIR', dirname( __FILE__ ).DS.'..'.DS );
}

if( !defined( 'GEDOOO_TEST_FILE' ) ) {
        define( 'GEDOOO_TEST_FILE', GEDOOO_PLUGIN_DIR.'Test'.DS );
}

if( !defined( 'ROOTPATH' ) ) {
    define( 'ROOTPATH', dirname(__FILE__) );
}


Log::setConfig('default', function () {
    $output = "[%datetime%] %context% %channel%.%level_name%: %message%\n";
    $formatter = new LineFormatter($output);

    $streamHandler = new StreamHandler(__DIR__.'/logs/app.log', Logger::INFO);
    $streamHandler->setFormatter($formatter);

    $logger = new Logger('FusionConv');
    $logger->pushHandler($streamHandler);

    return $logger;
});

Log::drop('debug');
Log::drop('error');
