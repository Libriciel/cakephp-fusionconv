<?php

namespace FusionConv\Test\TestCase\Utility;

use Cake\TestSuite\TestCase;
use phpgedooo_client\GDO_PartType;
use phpgedooo_client\GDO_FieldType;
use phpgedooo_client\GDO_IterationType;
use FusionConv\Utility\FusionConvBuilder;

require_once( dirname( __FILE__ ).DS.'..'.DS.'..'.DS.'..'.DS.'Config'.DS.'bootstrap.php' );

/**
 * FusionConvBuilderTest class...
 *
 * @package FusionConv
 * @subpackage tests.TestCase.Utility
 */
class FusionConvBuilderTest extends TestCase
{
	/**
	 * FusionConvBuilder::main() method test
	 */
	public function testMain() {
		$data = array(
			'User' => array(
				'id' => 4,
				'username' => 'cbuffin',
				'birthday' => '1979-01-24',
			)
		);
		$types = array(
			'User.id' => 'number',
			'User.username' => 'text',
			'User.birthday' => 'date',
		);
		$correspondances = array(
			'user_id' => 'User.id',
			'user_username' => 'User.username',
			'user_birthday' => 'User.birthday',
		);

		$MainPart = new GDO_PartType();
		$result = FusionConvBuilder::main( $MainPart, $data, $types, $correspondances );

		$expected = array(
			new GDO_FieldType( 'user_id', '4', 'number' ),
			new GDO_FieldType( 'user_username', 'cbuffin', 'text' ),
			new GDO_FieldType( 'user_birthday', '1979-01-24', 'date' )
		);
		$this->assertEquals( $result->field, $expected, var_export( $result->field, true ) );
	}

	/**
	 * FusionConvBuilder::iteration() method test
	 */
	public function testIteration() {
		$data = array(
			array(
				'User' => array(
					'id' => 4,
					'username' => 'cbuffin',
					'birthday' => '1979-01-24',
				)
			),
			array(
				'User' => array(
					'id' => 5,
					'username' => 'pmason',
					'birthday' => '1982-03-12',
				)
			)
		);
		$types = array(
			'User.id' => 'number',
			'User.username' => 'text',
			'User.birthday' => 'date',
		);
		$correspondances = array(
			'user_id' => 'User.id',
			'user_username' => 'User.username',
			'user_birthday' => 'User.birthday',
		);

		$MainPart = new GDO_PartType();
		// $iterationName = 'IterationName';
		$result = FusionConvBuilder::iteration( $MainPart, $iterationName, $data, $types, $correspondances );

		$expected = new GDO_IterationType( $iterationName );
		$expected->addPart( FusionConvBuilder::main( new GDO_PartType(), $data[0], $types, $correspondances ) );
		$expected->addPart( FusionConvBuilder::main( new GDO_PartType(), $data[1], $types, $correspondances ) );
		$expected = array( $expected );

		$this->assertEquals( $result->iteration, $expected, var_export( $result->iteration, true ) );
	}
}