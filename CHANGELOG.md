# ChangeLog

Toutes les modifications apportées au projet seront documentées dans ce fichier.

Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/) et adhère aux principes du [Semantic Versioning](http://semver.org/).

## [4.1.0] - 03/10/2023

### Ajouts

- Ajout de la possibilité d'utiliser le service de conversion unoserver

## [4.0.0] - 31/07/2023

### Modifié

- Mise à jour des librairies utilisées (libriciel/phpgedooo_client: 3.0.0, cakephp/cakephp 4.4.15)

## [3.0.0] - 12/01/2021

### Ajouts
- Prise en compte de Cakephp v3
- Changement de nom

## [2.0.2] - 09/01/2020

### Correction
- modification du nommage pour composer v1.9

## [2.0.1] - 12/06/2017

### Correction
- Correction de la fonction de convertion xml RPC2 (_xmlRpc2Convert) : Utility/Converter/FusionConvConverterCloudooo.php

## [2.0.0] - 08/03/2017

### Ajouts
- Utilisation des namespaces pour les dépendances
- Mise en place du fork phpgedooo_client
- Compatibilité PHP 7

### Suppression
- Suppression des rétrocompatibilités pour des versions précédentes de la librairie php de gedooo


## [1.0.0] - 15/01/2016

### Ajouts
- Mise en production

## [0.9.0]

### Ajouts
- Création du plugin CakePHP FusionConv

[Unreleased]: https://gitlab.libriciel.fr/CakePHP/FusionConv/compare/2.0.0...HEAD
[2.0.0]: https://gitlab.libriciel.fr/CakePHP/FusionConv/compare/1.0.0...2.0.0
[1.0.0]: https://gitlab.libriciel.fr/CakePHP/FusionConv/compare/0.0.9...1.0.0
[0.0.9]: https://gitlab.libriciel.fr/CakePHP/FusionConv/tags/0.0.9
